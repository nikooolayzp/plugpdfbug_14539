/*
 * Copyright (C) 2013 ePapyrus, Inc. All rights reserved.
 *
 * This file is part of the PlugPDF Sample Project whose source code is provided to show how to
 * use the PlugPDF SDK.
 */

package com.epapyrus.plugpdf.sample;

import java.io.File;
import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.epapyrus.plugpdf.core.PDFDocument;
import com.epapyrus.plugpdf.core.PlugPDFException.WrongPassword;
import com.epapyrus.plugpdf.sample.documentView.ReaderWithControllerActivity;
import com.epapyrus.plugpdf.sample.documentViewWithoutController.ReaderActivity;

/**
 * Fetches and displays the PDF document(s) on the DIRECTORY_DOWNLOADS path
 * (i.e. '/mnt/sdcard/Download') in ListView.
 *  
 * @author ePapyrus
 * @see <a target="_blank" href="https://developer.android.com/reference/android/widget/ListView.html">android.widget.ListView</a>
 */

public class PDFListActivity extends Activity implements OnItemClickListener {

	protected File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
	private LinearLayout mLayout;
	private String intentType;
	
	/**
	 * Sets the activity content, creating a layout.
	 * 
	 * @see <a target="_blank" href="http://developer.android.com/reference/android/app/Activity.html#onCreate(android.os.Bundle)">android.app.Activity.onCreate(android.os.Bundle)</a>
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		intentType = getIntent().getStringExtra("TYPE");

		mLayout = new LinearLayout(this);
		mLayout.setOrientation(LinearLayout.VERTICAL);
		setContentView(mLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
		refreshList();
	}
	
	/**
	 * Fetches the files from the directory, adds a click listener and lists the PDF document(s) in
	 * {@link ListView}. If there are no files available and the intent type doesn't start
	 * with "Reader", the user is asked to store a document; otherwise, opens the sample PDF just
	 * after a screen touch takes place.
	 *
	 * @see <a target="_blank" href="https://developer.android.com/reference/android/widget/ListView.html">android.widget.ListView</a>
	 * @see <a target="_blank" href="http://developer.android.com/reference/android/content/Intent.html">android.content.Intent</a>
	 */
	protected void refreshList() {
		
		mLayout.removeAllViews();
		

			
			TextView textView = new TextView(this);
			final String filename1 = "ChisholmEdda.pdf";
			String text= " If you touch to this screen will open "+filename1;
			
			if (willOpenReader()) {
				textView.setOnClickListener(new OnClickListener() {
					 
					@Override
					public void onClick(View v) {
						Intent readerIntent = getIntent(intentType);
						readerIntent.putExtra(ReaderWithControllerActivity.FILE_NAME_TAG, filename1);
						startActivity(readerIntent);
					}
				});
			}
			
			textView.setText(text);
			textView.setTextSize(20.f);
			mLayout.addView(textView, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

			TextView textView2 = new TextView(this);
			final String filename2 = "Gone_With_the_Wind.pdf";
			String text2= " If you touch to this screen will open "+filename2;
			if (willOpenReader()) {
				textView2.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent readerIntent = getIntent(intentType);
						readerIntent.putExtra(ReaderWithControllerActivity.FILE_NAME_TAG, filename2);
						startActivity(readerIntent);
					}
				});
			}
			textView2.setText(text2);
			textView2.setTextSize(20.f);
			mLayout.addView(textView2, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));


			TextView textView3 = new TextView(this);
			final String filename3 = "QA_Watermarks.pdf";
			String text3= " If you touch to this screen will open "+filename3;
			if (willOpenReader()) {
				textView3.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent readerIntent = getIntent(intentType);
						readerIntent.putExtra(ReaderWithControllerActivity.FILE_NAME_TAG, filename3);
						startActivity(readerIntent);
					}
				});
			}
			textView3.setText(text3);
			textView3.setTextSize(20.f);
			mLayout.addView(textView3, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));


			TextView textView4 = new TextView(this);
			final String filename4 = "Sample08InvisibleSignature.pdf";
			String text4= " If you touch to this screen will open "+filename4;
			if (willOpenReader()) {
				textView4.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent readerIntent = getIntent(intentType);
						readerIntent.putExtra(ReaderWithControllerActivity.FILE_NAME_TAG, filename4);
						startActivity(readerIntent);
					}
				});
			}
			textView4.setText(text4);
			textView4.setTextSize(20.f);
			mLayout.addView(textView4, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

			TextView textView5 = new TextView(this);
			final String filename5 = "Sample12Invisiblecertifysignaturenochangepermission.pdf";
			String text5= " If you touch to this screen will open "+filename5;
			if (willOpenReader()) {
				textView5.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent readerIntent = getIntent(intentType);
						readerIntent.putExtra(ReaderWithControllerActivity.FILE_NAME_TAG, filename5);
						startActivity(readerIntent);
					}
				});
			}
			textView5.setText(text5);
			textView5.setTextSize(20.f);
			mLayout.addView(textView5, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));


	}
	
	/* (non-Javadoc)
	 * @see <a target="_blank" href="https://developer.android.com/reference/android/widget/AdapterView.OnItemClickListener.html#onItemClick%28android.widget.AdapterView%3C?%3E,%20android.view.View,%20int,%20long%29">android.widget.AdapterView.OnItemClickListener.onItemClick(android.widget.AdapterView, android.view.View, int, long)</a>
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int index, long longIndex) {
		Intent readerIntent = getIntent(intentType);
		readerIntent.putExtra("fileName", downloadDir.getAbsolutePath() + "/" + parent.getItemAtPosition(index));
		startActivity(readerIntent);
	}

	/**
	 * Checks the intent type, whether it starts with the keyword 'READER' or not
	 *
	 * @return boolean
	 * @see <a target="_blank" href="http://developer.android.com/reference/android/content/Intent.html">android.content.Intent</a>
	 */
	private boolean willOpenReader() {
		return intentType.startsWith("READER");
	}

	/**
	 * Given an intent type, determines and returns its corresponding activity
	 *
	 * @param intentType
	 * @return Intent of type {@link ReaderWithControllerActivity} or {@link ReaderActivity}, or null
	 * @see <a target="_blank" href="http://developer.android.com/reference/android/content/Intent.html">android.content.Intent</a>
	 * @see <a target="_blank" href="http://developer.android.com/reference/android/app/Activity.html">android.app.Activity</a>
	 */
	private Intent getIntent(String intentType) {
		
		if (intentType.equals("READER"))
			return new Intent(this, ReaderWithControllerActivity.class);
		
		else if (intentType.equals("READER_WITHOUT_CONTROLLER"))
			return new Intent(this, ReaderActivity.class);
		
		else 
			return null;
	}
}
